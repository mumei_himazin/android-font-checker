package info.mumei.fontchecker;

import android.graphics.Typeface;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


public class MainActivity extends ActionBarActivity {

	Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		GridView gridView = (GridView)findViewById(R.id.gridView);
		gridView.setNumColumns(2);
		gridView.setFastScrollEnabled(true);

		gridView.setAdapter(new CharAdapter());
    }

	@Override
	protected void onResume() {
		super.onResume();
		File dir = Environment.getExternalStoragePublicDirectory("font");
		if(!dir.exists())dir.mkdir();
		Toast.makeText(this,dir.getPath(),Toast.LENGTH_LONG).show();
		File font = new File(dir,"original.ttf");
		typeface = Typeface.createFromFile(font.getPath());
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

	private class CharAdapter extends BaseAdapter{

		LayoutInflater inflater;

		CharAdapter(){
			inflater = (LayoutInflater)MainActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return Character.MAX_CODE_POINT;
		}

		@Override
		public Character getItem(int position) {
			return (char)position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null)convertView = inflater.inflate(R.layout.char_layout,parent,false);
			assert convertView!=null;

			char c = getItem(position);

			((TextView)convertView.findViewById(R.id.code)).setText("0x"+Integer.toHexString(c));
			((TextView)convertView.findViewById(R.id.android)).setText(""+c);
			TextView font = (TextView)convertView.findViewById(R.id.font);
			font.setText(""+c);
			font.setTypeface(typeface);
			return convertView;
		}
	}

}
